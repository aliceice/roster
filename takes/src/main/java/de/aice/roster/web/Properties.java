package de.aice.roster.web;

/**
 * Properties.
 *
 * @author Eléna Ihde-Simon (elena.ihde-simon@posteo.de)
 * @version $Id$
 */
public interface Properties {

	/**
	 * Server port.
	 *
	 * @return port
	 */
	int port();
}
